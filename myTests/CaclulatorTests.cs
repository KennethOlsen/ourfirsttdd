﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using myCalculator;


namespace myTests
{
    public class CalculatorTests
    {
        // 4 tests for Calculator.

        [Fact]
        public void Add_addingNumbers()
        {
            //Arrange
            double num1 = 5;
            double num2 = 5;
            double expected = 10;

            //Act
            double actual = Calculator.Add(num1, num2);

            //Assert
            Assert.Equal(expected, actual);                    

        }

        [Theory]
        [InlineData(6, 4, 2)]
        [InlineData(10, 9, 4)]
        public void Subtract_subtracktNumbers(double num1, double num2, double expected)
        {
            double actual = Calculator.Subtract(num1, num2);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 2, 5)]
        [InlineData(3, 2, 8)]
        public void Divide_dividingNumbers(double num1, double num2, double expected)
        {
            double actual = Calculator.Divide(num1, num2);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mulitply_mulitplyingNumbers()
        {
            double num1 = 2;
            double num2 = 4;
            double expected = 8;

            double actual = Calculator.Multiply(num1, num2);

            Assert.Equal(expected, actual);

        }
        





    }
}
